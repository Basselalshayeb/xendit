const {promisify} = require('util');
const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database(':memory:');

db.run = promisify(db.run);
db.get = promisify(db.get);
db.all = promisify(db.all);
db.serialize = promisify(db.serialize);

module.exports = db;
