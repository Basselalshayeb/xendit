# Xendit Module Description

This project provide a module that keeps management of rideshare system.

## Prequists

-   Visual studio build tools 2015
-   Node.js (>8.6 and <= 10) && npm ^5.0.0

## Installing And Testing

-   npm i
-   npm test

## Main Features

-   Insert new ride into the database
-   Display all rides in the database
-   Display specific ride in the database

## REST API Doc
- All API are in post man collection doc: https://documenter.getpostman.com/view/2664951/Uz5FJcGT
- Or you can read them from this file.
### Fetch All Rides

#### Request

`GET /rides/`

    curl -i -H 'Accept: application/json' http://localhost:8010/rides/

#### Response

    - If there is rides:
    [
      {
          "rideID": 2,
          "startLat": 33,
          "startLong": 33,
          "endLat": 44,
          "endLong": 44,
          "riderName": "feafq",
          "driverName": "test2",
          "driverVehicle": "test3",
          "created": "2022-06-01 21:02:40"
      }
    ]
    - If there is no rides:
    {
      "error_code":"RIDES_NOT_FOUND_ERROR",
      "message":"Could not find any rides"
    }

### Insert New Ride

#### Request

`POST /rides/`

    curl -i -H 'Accept: application/json' -d 
    'start_lat=33&start_long=33&end_lat=44&end_long=44&rider_name=feafq&driver_name=test2&driver_vehicle=test3' POST http://localhost:8010/rides

#### Response

    - If all data is valid
    [
      {
        "rideID": 2,
        "startLat": 33,
        "startLong": 33,
        "endLat": 44,
        "endLong": 44,
        "riderName": "feafq",
        "driverName": "test2",
        "driverVehicle": "test3",
        "created": "2022-06-01 21:02:40"
      }
    ]
    - If there is a non valid data
    {
       error_code: 'VALIDATION_ERROR',
       message: 'Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively'
    }
    {
      error_code: 'VALIDATION_ERROR',
      message: 'Rider name must be a non empty string'
    }
    {
      error_code: 'VALIDATION_ERROR',
      message: 'Driver name must be a non empty string'
    }
    {
      error_code: 'VALIDATION_ERROR',
      message: 'Rider name must be a non empty string'
    }

### Fetch Ride By Id

#### Request

`GET /rides/:id`

    curl -i -H 'Accept: application/json' http://localhost:8010/rides/1

#### Response

    - If ride exists:
    [
      {
          "rideID": 2,
          "startLat": 33,
          "startLong": 33,
          "endLat": 44,
          "endLong": 44,
          "riderName": "feafq",
          "driverName": "test2",
          "driverVehicle": "test3",
          "created": "2022-06-01 21:02:40"
      }
    ]
    - If ride dose not exists:
    {
      "error_code": "RIDES_NOT_FOUND_ERROR",
      "message": "Could not find any rides"
    }
