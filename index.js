'use strict';

const port = 8010;

const db = require('./db/sqlite3');

const buildSchemas = require('./src/schemas');

(async () => {
  await db.serialize();
  buildSchemas(db);
  const app = require('./src/app')(db);
  app.listen(port, () =>
    console.log(`App started and listening on port ${port}`));
})();

