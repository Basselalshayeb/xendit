'use strict';
/* eslint-disable  */
const request = require('supertest');
const db = require('../db/sqlite3');

const app = require('../src/app')(db);
const buildSchemas = require('../src/schemas');

describe('API tests', () => {
  before((done) => {
    db.serialize((err) => {
      if (err) {
        return done(err);
      }

      buildSchemas(db);

      done();
    });
  });

  describe('GET /health', () => {
    it('should return health', (done) => {
      request(app)
          .get('/health')
          .expect('Content-Type', /text/)
          .expect(200, done);
    });
  });

  describe('GET /rides', () => {
    it('it should return no available ', (done) => {
      request(app)
          .get('/rides')
          .expect('Content-Type', /json/)
          .expect(200, done);
    });
  });

  describe('POST /rides', () => {
    it('it should return the new ride', (done) => {
      request(app)
          .post('/rides')
          .send({
            'start_lat': 44,
            'start_long': 33,
            'end_lat': 44,
            'end_long': 44,
            'rider_name': 'feafq',
            'driver_name': 'Bassel',
            'driver_vehicle': 'BMW',
          })
          .expect(200, done);
    });
  });

  describe('POST /rides', () => {
    it('it should return the new ride', (done) => {
      request(app)
          .post('/rides')
          .send({
            'start_lat': 44,
            'start_long': 33,
            'end_lat': 44,
            'end_long': 44,
            'rider_name': 'Feras',
            'driver_name': 'michale',
            'driver_vehicle': 'Mercedes',
          })
          .expect(200, done);
    });
  });

  describe('POST /rides', () => {
    it('it should return coordinates error', (done) => {
      request(app)
          .post('/rides')
          .send({
            'start_lat': 111,
            'start_long': 33,
            'end_lat': 44,
            'end_long': 44,
            'rider_name': 'Bassel',
            'driver_name': 'Bassel',
            'driver_vehicle': 'BMW',
          })
          .expect('Content-Type', /json/)
          .expect(200, done);
    });
  });

  describe('POST /rides', () => {
    it('it should return coordinates error', (done) => {
      request(app)
          .post('/rides')
          .send({
            'start_lat': 33,
            'start_long': 181,
            'end_lat': 44,
            'end_long': 44,
            'rider_name': 'Bassel',
            'driver_name': 'Bassel',
            'driver_vehicle': 'BMW',
          })
          .expect('Content-Type', /json/)
          .expect(200, done);
    });
  });

  describe('POST /rides', () => {
    it('it should return rider error', (done) => {
      request(app)
          .post('/rides')
          .send({
            'start_lat': 50,
            'start_long': 33,
            'end_lat': 44,
            'end_long': 44,
            'rider_name': '',
            'driver_name': 'Bassel',
            'driver_vehicle': 'BMW',
          })
          .expect('Content-Type', /json/)
          .expect(200, done);
    });
  });

  describe('POST /rides', () => {
    it('it should return rider error', (done) => {
      request(app)
          .post('/rides')
          .send({
            'start_lat': 50,
            'start_long': 33,
            'end_lat': 44,
            'end_long': 44,
            'rider_name': 'Bassel',
            'driver_name': '',
            'driver_vehicle': 'BMW',
          })
          .expect('Content-Type', /json/)
          .expect(200, done);
    });
  });

  describe('GET /rides', () => {
    it('it should return all rides', (done) => {
      request(app)
          .get('/rides')
          .expect('Content-Type', /json/)
          .expect(200, done);
    });
  });

  describe('GET /rides/1', () => {
    it('it should return ride with id 1', (done) => {
      request(app)
          .get('/rides/1')
          .expect('Content-Type', /json/)
          .expect(200, done);
    });
  });

  describe('GET /rides/\' or true;', () => {
    it('it should return error after closing the threat', (done) => {
      request(app)
          .get('/rides/\' or true;')
          .expect('Content-Type', /json/)
          .expect(200)
          .end((err, res) => {
            if(err) return done(err);
            if(res.body.error_code !== 'RIDES_NOT_FOUND_ERROR'){
              return done(new Error('Invalid Response'));
            }
            done();
          });
    });
  });

  describe('GET /error', () => {
    it('it should raise error', (done) => {
      request(app)
          .get('/error')
          .expect('Content-Type', /text/)
          .expect(500, done);
    });
  });
});
